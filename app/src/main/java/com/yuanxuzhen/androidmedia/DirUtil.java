package com.yuanxuzhen.androidmedia;

import android.os.Environment;

import java.io.File;

public class DirUtil {
    public static String getCacheDir() {
        String path = null;
        if (AndroidMediaApplication.getInstance().getExternalCacheDir() != null
                && (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable())) {
            //外部存储可用
            path = AndroidMediaApplication.getInstance().getExternalCacheDir().getPath();
        } else {
            //内部存储不可用
            path = AndroidMediaApplication.getInstance().getCacheDir().getPath();
        }
        return path;
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }
}
