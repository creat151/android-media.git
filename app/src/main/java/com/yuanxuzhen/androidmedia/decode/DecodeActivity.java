package com.yuanxuzhen.androidmedia.decode;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.yuanxuzhen.androidmedia.DirUtil;
import com.yuanxuzhen.androidmedia.audio.AudioActivity;
import com.yuanxuzhen.androidmedia.databinding.ActivityDecodeBinding;
import com.yuanxuzhen.androidmedia.databinding.ActivityMainBinding;
import com.yuanxuzhen.androidmedia.demux.DemuxActivity;
import com.yuanxuzhen.androidmedia.mediaplayer.MediaPlayerActivity;
import com.yuanxuzhen.androidmedia.video.CameraActivity;
import com.yuanxuzhen.testandroid.Yuan;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DecodeActivity extends AppCompatActivity {
    ActivityDecodeBinding mainBinding;
    ExecutorService mExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityDecodeBinding.inflate(getLayoutInflater());
        setContentView(mainBinding.getRoot());
        mExecutor = Executors.newCachedThreadPool();
        Log.e("DecodeActivity", "path="+ DirUtil.getCacheDir());
        mainBinding.aac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        AacDecoder aacDecoder = new AacDecoder();
                        String inAudioPath = DirUtil.getCacheDir() + File.separator + "in.mp4";
                        String outAudioPath = DirUtil.getCacheDir() + File.separator + "outAudio.pcm";
                        Log.e("DecodeActivity", inAudioPath + "");
                        Log.e("DecodeActivity", outAudioPath + "");
                        aacDecoder.decode(inAudioPath, outAudioPath);
                    }
                });

            }
        });

        mainBinding.videoDecode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        YuvDecoder decoder = new YuvDecoder();
                        String srcPath = DirUtil.getCacheDir() + File.separator + "in.mp4";
                        String outPath = DirUtil.getCacheDir() + File.separator + "outvideo.yuv";
                        decoder.decode(srcPath, outPath);
                    }
                });
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}