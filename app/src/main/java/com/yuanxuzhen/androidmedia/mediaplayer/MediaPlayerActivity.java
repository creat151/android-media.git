package com.yuanxuzhen.androidmedia.mediaplayer;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.yuanxuzhen.androidmedia.DirUtil;
import com.yuanxuzhen.androidmedia.ITimerListener;
import com.yuanxuzhen.androidmedia.YuanTimerTask;
import com.yuanxuzhen.androidmedia.audio.AudioActivity;
import com.yuanxuzhen.androidmedia.databinding.ActivityMainBinding;
import com.yuanxuzhen.androidmedia.databinding.ActivityMediaPlayerBinding;
import com.yuanxuzhen.androidmedia.demux.DemuxActivity;
import com.yuanxuzhen.androidmedia.video.CameraActivity;

import java.io.File;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MediaPlayerActivity extends AppCompatActivity
        implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnPreparedListener,
        ITimerListener {
    ActivityMediaPlayerBinding mainBinding;
    private MediaPlayer mediaPlayer;
    private String mediaPath;
    ExecutorService executorService;
    Timer mTimer;
    private void printLog(String msg){
        Log.i("MediaPlayerYuab", msg);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTimer = new Timer();
        executorService = Executors.newCachedThreadPool();
        mainBinding = ActivityMediaPlayerBinding.inflate(getLayoutInflater());
        setContentView(mainBinding.getRoot());
        mediaPlayer = new MediaPlayer();
        mediaPath = DirUtil.getCacheDir()  + File.separator + "in.mp4";
//        mediaPath = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
        mainBinding.surfaceView2.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder holder) {
                printLog("surfaceCreated");
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDisplay(mainBinding.surfaceView2.getHolder());
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

            }
        });

        mainBinding.set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            mediaPlayer.setDataSource(MediaPlayerActivity.this, Uri.parse(mediaPath));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        mainBinding.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            printLog("start 1111111");
                            mediaPlayer.prepare();
                            printLog("start 2222222");

                            mediaPlayer.start();
                            initTimer();
                            printLog("start 33333");

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


            }
        });

        mainBinding.resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        printLog("resume");

                        try{
                            mediaPlayer.start();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


            }
        });
        mainBinding.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printLog("pause");

                mediaPlayer.pause();
            }
        });

        mediaPlayer.setOnCompletionListener(this);

        mediaPlayer.setOnErrorListener(this);

        mediaPlayer.setOnPreparedListener(this);

    }

    @Override
    protected void onDestroy() {
        if(mediaPlayer != null){
            mediaPlayer.release();
        }
        cancleTimer();
        super.onDestroy();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        printLog("onCompletion");

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        printLog("onError what="+what + " extra="+extra);

        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        printLog("onPrepared");

    }

    @Override
    public void onTimer() {
        if(mediaPlayer == null){
            return;
        }
        if(!mediaPlayer.isPlaying()){
            return;
        }
        long duraction = mediaPlayer.getDuration();
        long curration = mediaPlayer.getCurrentPosition();
        String time = curration/1000 + "/" + duraction/1000;
        mainBinding.tvTime.post(new Runnable() {
            @Override
            public void run() {
                mainBinding.tvTime.setText(time);
            }
        });

    }

    private void  initTimer(){
        cancleTimer();
        mTimer = new Timer();
        YuanTimerTask timerTask = new YuanTimerTask(this);
        mTimer.schedule(timerTask, 0, 1000);
    }

    private void  cancleTimer(){
        if(mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
    }
}