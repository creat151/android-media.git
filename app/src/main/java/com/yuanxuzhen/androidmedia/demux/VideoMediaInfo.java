package com.yuanxuzhen.androidmedia.demux;

public class VideoMediaInfo {
    public String name;
    public String language; //语言
    public int width; //视频宽
    public int height; //视频高
    public long durationTime;//视频时间
    public int maxByteCount;
    public int colorFormat;
    public int frameRate;
    public int tileWidth;
    public int  tileHeight;
    public int gridRows;
    public int  gridColumns;
    public int trackIndex;

    @Override
    public String toString() {
        return "VideoMediaInfo{" +
                "name='" + name + '\'' +
                ", language='" + language + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", durationTime=" + durationTime +
                ", maxByteCount=" + maxByteCount +
                ", colorFormat=" + colorFormat +
                ", frameRate=" + frameRate +
                ", tileWidth=" + tileWidth +
                ", tileHeight=" + tileHeight +
                ", gridRows=" + gridRows +
                ", gridColumns=" + gridColumns +
                ", trackIndex=" + trackIndex +
                '}';
    }
}
