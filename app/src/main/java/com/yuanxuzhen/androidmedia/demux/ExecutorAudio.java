package com.yuanxuzhen.androidmedia.demux;

import android.media.MediaExtractor;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;

public class ExecutorAudio {

    public void extractAudio(String srcPath, String dstPath){
        AudioMediaInfo mediaInfo = MediaUtil.getAudioMeidaInfo(srcPath);
        FileOutputStream dstFileOutPutStream = null;
        File file = new File(dstPath);
        if(file.exists()){
            file.delete();
        }
        try{
            dstFileOutPutStream=new FileOutputStream(dstPath,true);
            MediaExtractor extractor = new MediaExtractor();
            extractor.setDataSource(srcPath);
            extractor.selectTrack(mediaInfo.trackIndex);
            ByteBuffer byteBuffer = ByteBuffer.allocate(100 * 1024);
            while (true){
                int readSampleCount = extractor.readSampleData(byteBuffer, 0);
                if(readSampleCount <= 0){
                    break;
                }
                byte[] buffer = new byte[readSampleCount];
                byteBuffer.get(buffer);
                byte[] adtsHeader = getAdtsHeader(readSampleCount,
                        mediaInfo.aacProfile,
                        mediaInfo.sampleRate,
                        mediaInfo.channelCount);
                dstFileOutPutStream.write(adtsHeader);
                dstFileOutPutStream.write(buffer);
                dstFileOutPutStream.flush();
                byteBuffer.clear();
                extractor.advance();
            }
            extractor.release();
            extractor = null;
            dstFileOutPutStream.close();

        }catch (Exception e){

        }finally {
        }
    }


    int getAudioObjType(int aactype)
    {
        //AAC HE V2 = AAC LC + SBR + PS
        //AAV HE = AAC LC + SBR
        //所以无论是 AAC_HEv2 还是 AAC_HE 都是 AAC_LC
        switch (aactype)
        {
            case 0:
            case 2:
            case 3:
                return aactype + 1;
            case 1:
            case 4:
            case 28:
                return 2;
            default:
                return 2;
        }
    }

    int getSampleRateIndex(int freq, int aactype)
    {

        int i = 0;
        int[] freq_arr = {
                96000, 88200, 64000, 48000, 44100, 32000,
                24000, 22050, 16000, 12000, 11025, 8000, 7350};

        //如果是 AAC HEv2 或 AAC HE, 则频率减半
        if (aactype == 28 || aactype == 4)
        {
            freq /= 2;
        }

        for (i = 0; i < 13; i++)
        {
            if (freq == freq_arr[i])
            {
                return i;
            }
        }
        return 4; //默认是44100
    }



    int getChannelConfig(int channels, int aactype)
    {
        //如果是 AAC HEv2 通道数减半
        if (aactype == 28)
        {
            return (channels / 2);
        }
        return channels;
    }


    /*AdtsHeader
    *AdtsHeader可能是7个字节也可能是9个字节，取决于
    *syncword 12bit  0xfff 总是0xfff代表一个帧的开始
    * ID：     1bit    代表MPEG的版本 MPEG-4: 0  MPGE2:1
    * Layer：  2bit     always: '00'
    *protection_absent:   1bit     不存在CRC是1（adtsheader就是7个字节）  存在CRC的是0（就是9个字节）
    *profile：   2bit 表示使用哪个级别的AAC，如01 Low Complexity(LC) -- AAC LC  profile的值等于 Audio Object Type的值减1. profile = MPEG-4 Audio Object Type - 1
    *sampling_frequency_index：4bit  采样率的下标
    *channel_configuration：3bit  声道数
    *
    * */




    private byte[] getAdtsHeader(int dataLen, int aactype, int frequency, int channels)
    {

        byte[] adtsHeader = new byte[7];

        int audio_object_type = getAudioObjType(aactype);
        int sampling_frequency_index = getSampleRateIndex(frequency, aactype);
        int channel_config = getChannelConfig(channels, aactype);

        Log.e("EXecutorAudio", "aot=" + audio_object_type + "freq_index=" + sampling_frequency_index + "channel=" + channel_config);

        int adtsLen = dataLen + 7;

        adtsHeader[0] = (byte) 0xff;      //syncword:0xfff                          高8bits
        adtsHeader[1] = (byte) 0xf0;      //syncword:0xfff                          低4bits
        adtsHeader[1] |= (0 << 3); //MPEG Version:0 for MPEG-4,1 for MPEG-2  1bit
        adtsHeader[1] |= (0 << 1); //Layer:0                                 2bits
        adtsHeader[1] |= 1;        //protection absent:1                     1bit

        adtsHeader[2] = (byte) ((audio_object_type - 1) << 6);            //profile:audio_object_type - 1                      2bits
        adtsHeader[2] |= (sampling_frequency_index & 0x0f) << 2; //sampling frequency index:sampling_frequency_index  4bits
        adtsHeader[2] |= (0 << 1);                               //private bit:0                                      1bit
        adtsHeader[2] |= (channel_config & 0x04) >> 2;           //channel configuration:channel_config               高1bit

        adtsHeader[3] = (byte) ((channel_config & 0x03) << 6); //channel configuration:channel_config      低2bits
        adtsHeader[3] |= (0 << 5);                    //original：0                               1bit
        adtsHeader[3] |= (0 << 4);                    //home：0                                   1bit
        adtsHeader[3] |= (0 << 3);                    //copyright id bit：0                       1bit
        adtsHeader[3] |= (0 << 2);                    //copyright id start：0                     1bit
        adtsHeader[3] |= ((adtsLen & 0x1800) >> 11);  //frame length：value   高2bits

        adtsHeader[4] = (byte)((adtsLen & 0x7f8) >> 3); //frame length:value    中间8bits
        adtsHeader[5] = (byte)((adtsLen & 0x7) << 5);   //frame length:value    低3bits
        adtsHeader[5] |= 0x1f;                             //buffer fullness:0x7ff 高5bits
        adtsHeader[6] = (byte) 0xfc;
        return adtsHeader;
    }




}
