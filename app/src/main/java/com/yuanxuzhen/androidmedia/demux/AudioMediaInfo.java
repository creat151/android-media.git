package com.yuanxuzhen.androidmedia.demux;

public class AudioMediaInfo {
    int bitRate;
    int pcmEncoding;
    int sampleRate;
    int channelCount;
    int isAdts;
    int keyChannelMask;
    int aacProfile;
    int trackIndex;

    @Override
    public String toString() {
        return "AudioMediaInfo{" +
                "bitRate=" + bitRate +
                ", pcmEncoding=" + pcmEncoding +
                ", sampleRate=" + sampleRate +
                ", channelCount=" + channelCount +
                ", isAdts=" + isAdts +
                ", keyChannelMask=" + keyChannelMask +
                ", aacProfile=" + aacProfile +
                ", trackIndex=" + trackIndex +
                '}';
    }
}
