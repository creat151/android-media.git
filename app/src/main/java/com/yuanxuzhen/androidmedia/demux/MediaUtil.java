package com.yuanxuzhen.androidmedia.demux;

import android.media.MediaExtractor;
import android.media.MediaFormat;

import java.io.IOException;

public class MediaUtil {

    public static int getTrackIndex(String targetTrack, String path) {
        MediaExtractor extractor = new MediaExtractor();//实例一个MediaExtractor
        try {
            extractor.setDataSource(path);//设置添加MP4文件路径
        } catch (IOException e) {
            e.printStackTrace();
        }
        int trackIndex = -1;
        int count = extractor.getTrackCount();//获取轨道数量
        for (int i = 0; i < count; i++) {
            MediaFormat mediaFormat = extractor.getTrackFormat(i);
            String currentTrack = mediaFormat.getString(MediaFormat.KEY_MIME);
            if (currentTrack.startsWith(targetTrack)) {
                trackIndex = i;
                break;
            }
        }
        return trackIndex;

    }


    public static AudioMediaInfo getAudioMeidaInfo(String path){
        int index = getTrackIndex("audio", path);
        if(index < 0){
            return null;
        }
        try{
            MediaExtractor extractor = new MediaExtractor();//实例一个MediaExtractor
            extractor.setDataSource(path);
            MediaFormat mediaFormat = extractor.getTrackFormat(index);
            AudioMediaInfo mediaINfo = new AudioMediaInfo();
            mediaINfo.trackIndex = index;
            try{
                mediaINfo.bitRate = mediaFormat.getInteger(MediaFormat.KEY_BIT_RATE);//获取比特
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
//                mediaINfo.pcmEncoding = mediaFormat.getInteger(MediaFormat.KEY_PCM_ENCODING);//PCM-编码 模拟信号编码
                mediaINfo.sampleRate =  mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                mediaINfo.channelCount = mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
            }catch (Exception e){
                e.printStackTrace();
            }


            try{
                mediaINfo.isAdts = mediaFormat.getInteger(MediaFormat.KEY_IS_ADTS);
            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                mediaINfo.keyChannelMask = mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_MASK);//图块分辨率
            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                mediaINfo.aacProfile = mediaFormat.getInteger(MediaFormat.KEY_AAC_PROFILE);//图块分辨率
            }catch (Exception e){
                e.printStackTrace();
            }
            return mediaINfo;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static VideoMediaInfo getVideoMediaInfo(String path){
        int index = getTrackIndex("video", path);
        if(index < 0){
            return null;
        }
        try{
            MediaExtractor extractor = new MediaExtractor();//实例一个MediaExtractor
            extractor.setDataSource(path);
            MediaFormat mediaFormat = extractor.getTrackFormat(index);
            VideoMediaInfo videoMediaInfo = new VideoMediaInfo();
            videoMediaInfo.name = mediaFormat.getString(MediaFormat.KEY_MIME);

            videoMediaInfo.trackIndex = index;
            videoMediaInfo.language = mediaFormat.getString(MediaFormat.KEY_LANGUAGE);//获取语言格式内容
            videoMediaInfo.width = mediaFormat.getInteger(MediaFormat.KEY_WIDTH);
            videoMediaInfo.height = mediaFormat.getInteger(MediaFormat.KEY_HEIGHT);
            videoMediaInfo.durationTime = mediaFormat.getLong(MediaFormat.KEY_DURATION);

            videoMediaInfo.maxByteCount = mediaFormat.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE);//获取视频缓存输出的最大大小
            try{
                Integer bitRate = mediaFormat.getInteger(MediaFormat.KEY_BIT_RATE);//获取比特
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                videoMediaInfo.colorFormat = mediaFormat.getInteger(MediaFormat.KEY_COLOR_FORMAT);//颜色格式
            }catch (Exception e){
                e.printStackTrace();
            }

            try{
                videoMediaInfo.frameRate = mediaFormat.getInteger(MediaFormat.KEY_FRAME_RATE);//帧率

            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                videoMediaInfo. tileWidth = mediaFormat.getInteger(MediaFormat.KEY_TILE_WIDTH);//图块分辨率

            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                videoMediaInfo. tileHeight = mediaFormat.getInteger(MediaFormat.KEY_TILE_HEIGHT);//图块分辨率

            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                videoMediaInfo.gridRows = mediaFormat.getInteger(MediaFormat.KEY_GRID_ROWS);//网格行

            }catch (Exception e){
                e.printStackTrace();
            }
            try{
                videoMediaInfo.gridColumns = mediaFormat.getInteger(MediaFormat.KEY_GRID_COLUMNS);//网格列

            }catch (Exception e){
                e.printStackTrace();
            }

            return videoMediaInfo;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;

    }


    public  static int getMediaCount(String path){
        try{
            MediaExtractor extractor = new MediaExtractor();//实例一个MediaExtractor
            extractor.setDataSource(path);
            int count = extractor.getTrackCount();//获取轨道数量
            return count;

        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;

    }

}
