package com.yuanxuzhen.androidmedia.demux;

import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.yuanxuzhen.androidmedia.DirUtil;
import com.yuanxuzhen.androidmedia.databinding.ActivityDemuxBinding;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DemuxActivity extends AppCompatActivity {
    ActivityDemuxBinding mainBinding;
    ExecutorService mExecutorService;
    private String mediaPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityDemuxBinding.inflate(getLayoutInflater());
        setContentView(mainBinding.getRoot());
        mExecutorService = Executors.newCachedThreadPool();
        mediaPath = DirUtil.getCacheDir() + File.separator + "in.mp4";
        mainBinding.print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                          /*打印通道信息*/
                        ExecutorAudio executorVideo = new ExecutorAudio();
                        executorVideo.extractAudio(mediaPath, DirUtil.getCacheDir() + File.separator + "123.h264");
                    }
                });
            }
        });
    }

    private void printLog(String msg){
        Log.e("DemuxAcYuan", msg);
    }









}