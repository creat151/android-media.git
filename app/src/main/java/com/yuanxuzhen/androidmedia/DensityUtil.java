package com.yuanxuzhen.androidmedia;

import android.content.Context;

public class DensityUtil {
    private static float scale;

    public DensityUtil() {
    }

    public static int dip2px(Context context, float dpValue) {
        if (scale == 0.0F) {
            scale = context.getResources().getDisplayMetrics().density;
        }

        return (int) (dpValue * scale + 0.5F);
    }

    public static int px2dip(Context context, float pxValue) {
        if (scale == 0.0F) {
            scale = context.getResources().getDisplayMetrics().density;
        }

        return (int) (pxValue / scale + 0.5F);
    }

    public static float getScale(Context context) {
        if (scale == 0.0F) {
            scale = context.getResources().getDisplayMetrics().density;
        }
        return scale;
    }
}
