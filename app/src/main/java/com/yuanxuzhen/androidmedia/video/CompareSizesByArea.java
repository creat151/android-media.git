package com.yuanxuzhen.androidmedia.video;

import android.util.Size;

import java.util.Comparator;

public class CompareSizesByArea implements Comparator<Size> {
    @Override
    public int compare(Size lhs, Size rhs) {
        return lhs.getWidth() * lhs.getHeight() - rhs.getWidth() * rhs.getHeight();
    }
}
