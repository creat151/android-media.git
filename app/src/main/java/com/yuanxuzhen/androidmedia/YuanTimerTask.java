package com.yuanxuzhen.androidmedia;

import java.util.TimerTask;

public class YuanTimerTask extends TimerTask {
    private ITimerListener mITimerListener = null;

    public YuanTimerTask(ITimerListener timerListener) {
        this.mITimerListener = timerListener;
    }

    public void setmITimerListener(ITimerListener mITimerListener) {
        this.mITimerListener = mITimerListener;
    }

    @Override
    public void run() {
        if (mITimerListener != null) {
            mITimerListener.onTimer();
        }
    }
}
