package com.yuanxuzhen.androidmedia;

import android.app.Application;
import android.content.Context;
import android.util.Log;


public class AndroidMediaApplication extends Application {
    private static AndroidMediaApplication instance;
    @Override
    public void onCreate() {
        Log.e("JetPackDemo", "onCreate");
        instance = this;
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        Log.e("JetPackDemo", "attachBaseContext");
        super.attachBaseContext(base);
    }

    public static AndroidMediaApplication getInstance() {
        return instance;
    }
}
