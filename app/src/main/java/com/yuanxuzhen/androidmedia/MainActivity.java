package com.yuanxuzhen.androidmedia;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.yuanxuzhen.androidmedia.audio.AudioActivity;
import com.yuanxuzhen.androidmedia.databinding.ActivityMainBinding;
import com.yuanxuzhen.androidmedia.decode.DecodeActivity;
import com.yuanxuzhen.androidmedia.demux.DemuxActivity;
import com.yuanxuzhen.androidmedia.encode.EncodeActivity;
import com.yuanxuzhen.androidmedia.mediaplayer.MediaPlayerActivity;
import com.yuanxuzhen.androidmedia.opengl.OpenGLActivity;
import com.yuanxuzhen.androidmedia.record.RecordActivity;
import com.yuanxuzhen.androidmedia.record.RecordActivityV1;
import com.yuanxuzhen.androidmedia.video.CameraActivity;
import com.yuanxuzhen.testandroid.Yuan;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding mainBinding;
    List<ItemMainData> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        list = new ArrayList<>();
        list.add(new ItemMainData(ItemMainData.ITEM_AAC_TO_PCM, "解码"));
        list.add(new ItemMainData(ItemMainData.ITEM_OPEN_GL, "opengl"));
        list.add(new ItemMainData(ItemMainData.ITEM_ENCODE, "encode"));
        list.add(new ItemMainData(ItemMainData.ITEM_RECORD, "record"));

        setContentView(mainBinding.getRoot());
        MainAdapter mainAdapter = new MainAdapter();
        mainBinding.rvList.setLayoutManager(new LinearLayoutManager(this));
        mainBinding.rvList.addItemDecoration(CommItemDecoration.createVertical(this, getResources().getColor(R.color.white), DensityUtil.dip2px(this, 10)));
        mainBinding.rvList.setAdapter(mainAdapter);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view =  LayoutInflater.from(MainActivity.this).inflate(R.layout.item_main, parent, false);

            return new MainViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            MainViewHolder mainViewHolder = (MainViewHolder)holder;
            mainViewHolder.tvItem.setText(list.get(position).content);
            mainViewHolder.flItemRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    Class<?> cls = null;
                    switch (list.get(position).id){
                        case ItemMainData.ITEM_AAC_TO_PCM:
                            intent.setClass(MainActivity.this, DecodeActivity.class);
                            cls = DecodeActivity.class;
                            break;
                        case ItemMainData.ITEM_OPEN_GL:
                            cls = OpenGLActivity.class;
                            break;
                        case ItemMainData.ITEM_ENCODE:
                            cls = EncodeActivity.class;
                            break;
                        case ItemMainData.ITEM_RECORD:
                            cls = RecordActivity.class;
                            break;
                    }
                    intent.setClass(MainActivity.this, cls);
                    MainActivity.this.startActivity(intent);

                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class MainViewHolder extends RecyclerView.ViewHolder{
            public TextView tvItem;
            public FrameLayout flItemRoot;
            public MainViewHolder(@NonNull View itemView) {
                super(itemView);
                flItemRoot = itemView.findViewById(R.id.fl_item_root);
                tvItem = itemView.findViewById(R.id.tv_item);
            }
        }
    }

}