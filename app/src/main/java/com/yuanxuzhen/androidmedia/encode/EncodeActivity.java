package com.yuanxuzhen.androidmedia.encode;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.yuanxuzhen.androidmedia.DirUtil;
import com.yuanxuzhen.androidmedia.IHanlderCallback;
import com.yuanxuzhen.androidmedia.R;
import com.yuanxuzhen.androidmedia.ThreadUtils;
import com.yuanxuzhen.androidmedia.databinding.ActivityEncodeBinding;

import java.io.File;

public class EncodeActivity extends AppCompatActivity {
    ActivityEncodeBinding mBinding = null;
    private AacEncoder aacEncoder;
    private YuvEncoder yuvEncoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityEncodeBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        Log.e("EncodeActivity", "dir="+ DirUtil.getCacheDir());
        mBinding.aacEncode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(aacEncoder == null){
                    aacEncoder = new AacEncoder();
                }
                String srcPath = DirUtil.getCacheDir() + File.separator + "encode" + File.separator + "in.pcm";
                String dstPath = DirUtil.getCacheDir() + File.separator + "encode" + File.separator + "out.aac";
                mBinding.encodeStatus.setText("音频开始编码");
                aacEncoder.init(EncodeActivity.this,
                        44100,
                        2,
                        AudioFormat.ENCODING_PCM_16BIT,
                        srcPath,
                        dstPath,
                        new IHanlderCallback() {
                            @Override
                            public void onSuccess(Object... object) {
                                ThreadUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mBinding.encodeStatus.setText("音频编码成功");
                                        mBinding.tvPath.setText(dstPath);
                                    }
                                });
                            }

                            @Override
                            public void onFail(Object... object) {
                                ThreadUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mBinding.encodeStatus.setText("音频编码失败");
                                        mBinding.tvPath.setText("");

                                    }
                                });
                            }
                        }
                );
                aacEncoder.startEncording();
            }

        });

        mBinding.h264Encode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(yuvEncoder == null){
                    yuvEncoder = new YuvEncoder();
                }
                String srcPath = DirUtil.getCacheDir() + File.separator + "encode" + File.separator + "in.yuv";
                String dstPath = DirUtil.getCacheDir() + File.separator + "encode" + File.separator + "out.h264";
                mBinding.encodeStatus.setText("视频开始编码");
                yuvEncoder.init(EncodeActivity.this,
                        960,
                        540,
                        srcPath,
                        dstPath,
                        new IHanlderCallback() {
                            @Override
                            public void onSuccess(Object... object) {
                                ThreadUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mBinding.encodeStatus.setText("视频编码成功");
                                        mBinding.tvPath.setText(dstPath);
                                    }
                                });
                            }

                            @Override
                            public void onFail(Object... object) {
                                ThreadUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mBinding.encodeStatus.setText("视频编码失败");
                                        mBinding.tvPath.setText("");

                                    }
                                });
                            }
                        }
                );
                yuvEncoder.startEncording();
            }
        });

    }

    @Override
    protected void onDestroy() {
        if(aacEncoder != null){
            aacEncoder.release();
        }
        super.onDestroy();
    }
}