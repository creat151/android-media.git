package com.yuanxuzhen.androidmedia;

public class ItemMainData {
    public static final int ITEM_AAC_TO_PCM = 0;
    public static final int ITEM_OPEN_GL = 1;
    public static final int ITEM_ENCODE = 2;
    public static final int ITEM_RECORD = 3;

    public int id;
    public String content;

    public ItemMainData(int id, String content) {
        this.id = id;
        this.content = content;
    }
}
