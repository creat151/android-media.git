package com.yuanxuzhen.androidmedia;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.yuanxuzhen.androidmedia.databinding.ActivityAudioBinding;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class DemoPermissionActivity extends AppCompatActivity {
    ActivityAudioBinding mainBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = ActivityAudioBinding.inflate(getLayoutInflater());
        setContentView(mainBinding.getRoot());
        mainBinding.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DemoPermissionActivityPermissionsDispatcher.openAudioWithPermissionCheck(DemoPermissionActivity.this);
            }
        });
    }

    @NeedsPermission(Manifest.permission.RECORD_AUDIO)
    public void openAudio(){

    }


    @OnPermissionDenied(Manifest.permission.RECORD_AUDIO)
    public void  onDenied() {
        Toast.makeText(this,"录音权限拒绝", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.RECORD_AUDIO)
    public void   onNeverAskAgain() {
        Toast.makeText(this, "录音权限再不询问", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        DemoPermissionActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

}